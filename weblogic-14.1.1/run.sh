#!/bin/bash

# Solicitar los puertos
read -p "Ingrese el puerto del host (7001): " HOST_PORT
HOST_PORT=${HOST_PORT:-7001}

# Solicitar la contraseña de administrador
read -p "Ingrese la contraseña de administrador: " ADMIN_PASSWORD

# Solicitar la imagen de Docker
read -p "Ingrese la imagen de Docker (oracle/weblogic:14.1.1.0-generic-11): " DOCKER_IMAGE
DOCKER_IMAGE=${DOCKER_IMAGE:-oracle/weblogic:14.1.1.0-generic-11}

# Ejecutar el comando docker run
docker run -d --name wlsadmin --hostname wlsadmin -p $HOST_PORT:7001 -e ADMINISTRATION_PORT_ENABLED=false -e ADMIN_PASSWORD=$ADMIN_PASSWORD $DOCKER_IMAGE


echo "El contenedor se ha iniciado correctamente."
