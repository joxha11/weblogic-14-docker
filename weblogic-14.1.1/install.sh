#!/bin/bash

# Solicitar el nombre de usuario y contraseña para domain.properties
read -p "Ingrese el nombre de usuario para domain.properties: " USERNAME
read -p "Ingrese la contraseña para domain.properties: " PASSWORD

# Crear el contenido para domain.properties
echo "username=$USERNAME" > ./properties/domain.properties
echo "password=$PASSWORD" >> ./properties/domain.properties
chmod 777 -R ./properties/*
# Ejecutar el comando buildDockerImage.sh
./buildDockerImage.sh -v 14.1.1.0 -g -j 11 -s

# Verificar si la construcción fue exitosa antes de eliminar el archivo domain.properties
if [ $? -eq 0 ]; then
    echo "La construcción de la imagen Docker fue exitosa."
    # Eliminar el archivo domain.properties
#    rm properties/domain.properties
    echo "El archivo domain.properties se ha eliminado."
else
    echo "La construcción de la imagen Docker ha fallado."
fi
