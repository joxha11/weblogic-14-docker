# Instalación de WebLogic 14 en Docker

Este README proporciona instrucciones detalladas sobre cómo instalar WebLogic 14 en Docker.

## Prerrequisitos

Antes de comenzar, asegúrate de tener instalado lo siguiente:

- Docker
- Para Windows **WSL (Windows Subsystem for Linux)**

## Pasos de instalación

1. **Clonar el repositorio**

    ```
    git clone https://gitlab.com/joxha11/weblogic-14-docker
    ```

### Descargar archivos de OracleJava y WebLogic

2. **Descargar Oracle JDK 11.0.21**

    Descarga el archivo `jdk-11.0.21_linux-x64_bin.tar.gz` desde [el sitio oficial de Oracle](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html) y reemplázalo con el archivo de mismo nombre en la carpeta `OracleJava` de este repositorio.

3. **Descargar WebLogic 14.1.1**

    Descarga el archivo correspondiente a weblogic 14.1.1.0.0  desde [enlace_del_repositorio](https://edelivery.oracle.com/osdc/faces/Home.jspx) respectivamente generalmente tiene nombre similar a 45939.zip luego, renombrar el archivo `fmw_14.1.1.0.0_wls_lite_Disk1_1of1.zip` y reemplzar con el que esta en la carpeta `weblogic-14.1.1` de este repositorio. Luego, descomprime el archivo y renómbralo como `fmw_14.1.1.0.0_wls_lite_generic.jar` reemplazando el existente descargado de git.

* Nota: el paso 2 y 3 son obligatorios y necesarios debido que Github no soporta mayores a 100mb.

4. **Ingresar a la carpeta OracleJava**

    ```
    cd OracleJava
    ```

5. **Instalación en Linux**

    Ejecuta el script de instalación:

    ```
    ./install.sh
    ```

    Asegúrate de que se haya creado la imagen `oracle/jdk`.

6. **Instalación en Windows**

    Ejecuta el script de instalación:

    ```
    install.bat
    ```

    Verifica si se ha creado la imagen `oracle/jdk`.

7. **Ingresar a la carpeta weblogic-14.1.1**

    ```
    cd ../weblogic-14.1.1
    ```

8. **Instalación en Linux**

    Ejecuta el script `Install.sh` y completa los datos requeridos cuando se te solicite.

    ```
    ./Install.sh
    ```

    Posteriormente, ejecuta el script `run.sh` y completa los datos requeridos.

    ```
    ./run.sh
    ```

9. **Instalación en Windows**

    Debes ejecutar los comandos Linux dentro de WSL (Windows Subsystem for Linux).

## Notas adicionales

- Asegúrate de proporcionar los datos requeridos durante la instalación según las indicaciones.
- Los scripts de instalación pueden requerir permisos de ejecución. Si es necesario, otórgalos utilizando `chmod +x script_name.sh`.
- Verifica que los contenedores de Docker estén en funcionamiento después de ejecutar los scripts.
- ¡Disfruta utilizando WebLogic 14 en Docker!


---

Este README fue creado por Joshuar Nájera De León - [2024].


# Installing WebLogic 14 on Docker

This README provides detailed instructions on how to install WebLogic 14 on Docker.

## Prerequisites

Before getting started, make sure you have the following installed:

- Docker
- For Windows, **WSL (Windows Subsystem for Linux)**

## Installation Steps

1. **Clone the Repository**

    ```
    git clone https://github.com/ClaroCENAM/weblogic14-docker.git
    ```

### Download OracleJava and WebLogic Files

2. **Download Oracle JDK 11.0.21**

    Download the `jdk-11.0.21_linux-x64_bin.tar.gz` file from [the official Oracle site](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html) and replace it with the file of the same name in the `OracleJava` folder of this repository.

3. **Download WebLogic 14.1.1**

    Download the file corresponding to WebLogic 14.1.1.0.0 from [repository_link](https://edelivery.oracle.com/osdc/faces/Home.jspx), generally having a name similar to 45939.zip, then rename the file to `fmw_14.1.1.0.0_wls_lite_Disk1_1of1.zip` and replace the one in the `weblogic-14.1.1` folder of this repository. Next, unzip the file and rename it as `fmw_14.1.1.0.0_wls_lite_generic.jar`, replacing the existing one downloaded from Git.

*Note: Steps 2 and 3 are mandatory and necessary because GitHub does not support files larger than 100MB.*

4. **Navigate to the OracleJava folder**

    ```
    cd OracleJava
    ```

5. **Installation on Linux**

    Run the installation script:

    ```
    ./install.sh
    ```

    Ensure that the `oracle/jdk` image has been created.

6. **Installation on Windows**

    Run the installation script:

    ```
    install.bat
    ```

    Verify that the `oracle/jdk` image has been created.

7. **Navigate to the weblogic-14.1.1 folder**

    ```
    cd ../weblogic-14.1.1
    ```

8. **Installation on Linux**

    Run the `Install.sh` script and provide the required details when prompted.

    ```
    ./Install.sh
    ```

    Then, execute the `run.sh` script and provide the required details.

    ```
    ./run.sh
    ```

9. **Installation on Windows**

    You need to execute the Linux commands within WSL (Windows Subsystem for Linux).

## Additional Notes

- Make sure to provide the required data during installation as per the instructions.
- The installation scripts may require execution permissions. If needed, grant them using `chmod +x script_name.sh`.
- Verify that the Docker containers are up and running after executing the scripts.
- Enjoy using WebLogic 14 on Docker!

---

This README was created by Joshuar Nájera De León - [2024].